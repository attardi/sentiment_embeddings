#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Language modeling networks used by the trainer."""

import theano
from theano import tensor as T

from word2embeddings.nn.networks import Network, StackedBiasedHidden
from word2embeddings.nn.layers import EmbeddingLayer, HingeLayer


class WordPhraseNetwork(Network):
  """Model to distinguish between corrupted phrases and observed ones."""

  def __init__(self, name="WordPhrase", word_shape=None, word_proj=1,
               hidden_layers=[1]):
    super(WordPhraseNetwork, self).__init__(name=name)
    vocab_size, word_size = word_shape
    layers = [word_proj * word_size]
    layers.extend(hidden_layers)
    layers.append(1)
    self.word_embedding = EmbeddingLayer(name='w_embedding', shape=word_shape)
    self.word_stack = StackedBiasedHidden(name='w_stack', layers=layers)
    self.loss = HingeLayer(name='loss')

    self.layers = [self.word_embedding, self.word_stack, self.loss]

  def link(self, inputs):
    self.inputs = inputs
    observed_phrases = inputs[0]
    corrupted_phrases = inputs[1]
    observed_words = self.word_embedding.link([observed_phrases])[0]
    observed_scores = self.word_stack.link([observed_words])[0]
    corrupted_scores = theano.clone(observed_scores,
                                    {observed_phrases: corrupted_phrases})
    self.outputs = self.loss.link([observed_scores, corrupted_scores])
    return self.outputs

  def get_word_embeddings(self):
    return self.word_embedding.weights.get_value()

